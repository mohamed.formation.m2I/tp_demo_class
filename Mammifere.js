import EtreVivant from "./EtreVivant.js";

export default class Mammifere extends EtreVivant {
    constructor(nom, type, age) {
        super(nom,type);
        this.age = age;
        this.heartRate = false;
        // this.Naissance();
    }

    Naissance(){
        super.Naissance();
        console.log("Naissance après gestation...");
        this.heartRate=true;
    }

    Mort(){
        console.log("Mon coeur s'arrête de battre !!");
        this.heartRate = false;
    }


}