export default class Person {

    static count = 0;

    constructor(firstName, lastName, age, country, city) {
        this.id = ++Person.count;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.country = country;
        this.city = city;
        this.score = 0;
        this.skills = []
    }


    getFullName() {
        const fullName = this.firstName + ' ' + this.lastName;
        return fullName;
    }

    get getScore() {
        return this.score;
    }

    set setScore(score) {
        this.score += score;
    }

   static favoriteSkill(){
    const skills = ['HTML','CSS','JS','React','Angular','Python','Java'];
    const index = Math.floor(Math.random() * skills.length);
    return skills[index];
   }


}