import Chien from "./Chien.js";
import Fleur from "./Fleur.js";
import Person from "./Person.js"


let medor = new Chien("Medor", "Berger Allemand", 12);
let milou = new Chien("Milou", "Pitbull");
let filou = new Fleur("filou", "Rose");
let filou2 = new Fleur("filou2", "Tulipe");
let titou = new Chien();

medor.Aboyer();

console.log("Le nom du chien est : " + medor.nom + " et il a " + medor.age);
medor.age = 45;
console.log(medor.age);
console.log(titou);
titou.nom = "Titou";
console.log(titou);


console.log("Le nom de la fleur est : " + filou.nom + " et elle a un id " + filou.id);
console.log("Le nom de la fleur est : " + filou2.nom + " et elle a un id " + filou2.id);

let etresVivant = [medor, milou, filou, titou];

// for (const etre of etresVivant) {

//     console.log("-----"+ etre.nom + "------");
//     etre.Respiration();
//     etre.Localisation();
//     etre.Mort();    
//     console.log("-----------");

// }


//let listeChien = etresVivant.filter(etre => etre instanceof Chien);

//listeChien.forEach(x => console.log(x.nom));

//let fleur = etresVivant.find(etre => etre instanceof Fleur);

//console.log(fleur);

//etresVivant.map((etre) => {
//  etre.nom = etre.nom + "" + "lol";
//});

//etresVivant.forEach(x => console.log(x.nom));

// etresVivant.forEach(etre => {
//     if (etre.nom.startsWith('M')) {
//         console.log(etre);
//     }
//     if (etre.type === "Berger Allemand") {

//         console.log("Je suis un berger Allemand");
//     }

// })



// const hero = {
//     name : "Batman",
//     realName : "Bruce Wayne"
// };

// const {name, realName} = hero;

// console.log(hero.name);
// console.log(name);


let person1 = new Person("Abdallah", "Jones", 31, "France", "Calais");
let person2 = new Person("Alain", "Deloin", 24, "France", "Roubaix");

console.log(person1.id);
console.log(person2.id);

console.log(Person.favoriteSkill());
console.log(Person.favoriteSkill());

const nom = prompt("Donne moi ton nom ? ");
const prenom = prompt("Donne moi ton prenom ? ");
const age = Number(prompt("Donne moi ton age ? "));
const pays = prompt("Donne moi ton pays ? ");
const ville = prompt("Donne ta ville ? ");

let person3 = new Person(nom, prenom, age, pays, ville);

localStorage.setItem("person",JSON.stringify(person3));

let person4 = JSON.parse(localStorage.getItem("person"));
firstName, lastName, age, country, city
document.getElementById("nom").textContent = person4.firstName;
document.getElementById("prenom").textContent = person4.lastName;
document.getElementById("age").textContent = person4.age;
document.getElementById("pays").textContent = person4.country;
document.getElementById("ville").textContent = person4.city;

